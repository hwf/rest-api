This repository is supposed to store the REST API specifications for the Insidious Report Project applications.

- The API Specification follows the [OpenAPI Specification](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md)
- API Responses payloads follows the [JSON API](http://jsonapi.org/)

